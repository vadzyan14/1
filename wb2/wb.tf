provider "aws" {
    region = "eu-central-1"
}

resource "aws_instance" "wb_server" {
    ami = "ami-0a1ee2fb28fe05df3"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.dynamicS_G.id]
    user_data = templatefile("user_data.sh.tpl", {
        f_name = "Vadim"
        l_name = "Volynets"
        names = ["Mikl", "Villius", "Mila"]
    })
}

 resource "aws_security_group" "dynamicS_G" {
    description = "first_dynamic_SG"    
 

 dynamic "ingress" {
for_each = ["9090", "8080", "443", "5858", "4040", "3030", "80", "22"]
    content{
         from_port = ingress.value
    to_port = ingress.value
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
}
 }

 egress {
    from_port = 0
   to_port = 0
    protocol = "-1"
   cidr_blocks = [ "0.0.0.0/0" ]
 } 
 
}

