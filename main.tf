provider "aws" {}
resource "aws_s3_bucket" "BucketB" {
   bucket = "vadim-user-project-1-first-bucket"
}
resource "aws_ssm_parameter" "foo" {
  name  = "/users/bucket/name"
  type  = "String"
  value = aws_s3_bucket.BucketB.id
}
resource "aws_ssm_parameter" "foo1" {
  name  = "/users/bucket/arn"
  type  = "String"
  value = aws_s3_bucket.BucketB.arn
}

output "Bucket_name" {
  value = aws_s3_bucket.BucketB.arn
}
output "DynamoDB_ARN" {
  value = aws_dynamodb_table.Users.arn
}

resource "aws_dynamodb_table" "Users" {
    name = "Users"
    hash_key       = "name"
    range_key = "email"
billing_mode = "PAY_PER_REQUEST"
attribute {
    name = "name"
    type = "S"
  }
attribute {
  name = "email"
  type ="S"
}


}
