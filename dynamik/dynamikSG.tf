provider "aws" {
    region = "eu-central-1"
  }

  resource "aws_security_group" "dynamicSG" {
    description = "first_dynamic_SG"    
 

 dynamic "ingress" {
for_each = ["9090", "8080", "443", "5858", "4040", "3030"]
    content{
         from_port = ingress.value
    to_port = ingress.value
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
}
 }

 ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
 } 
 egress {
    from_port = 0
    to_port = 0
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
 }
 tags = {
   Name = "DYNAMIC"
 }
  }

  