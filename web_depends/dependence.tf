provider "aws" {
  region = "eu-central-1"
}

resource "aws_instance" "web_1" {
  instance_type          = "t2.micro"
  ami                    = "ami-0a1ee2fb28fe05df3"
  vpc_security_group_ids = [aws_security_group.webD.id]
  tags = {
    Name = "WebD"
  }
  depends_on = [
    aws_instance.web_3
  ]
}

resource "aws_instance" "web_2" {
  instance_type          = "t2.micro"
  ami                    = "ami-0a1ee2fb28fe05df3"
  vpc_security_group_ids = [aws_security_group.webD.id]
  tags = {
    Name = "app"
  }
  depends_on = [
    aws_instance.web_3
  ]
}

resource "aws_instance" "web_3" {
  instance_type          = "t2.micro"
  ami                    = "ami-0a1ee2fb28fe05df3"
  vpc_security_group_ids = [aws_security_group.webD.id]
  tags = {
    Name = "DB"
  }
}

resource "aws_security_group" "webD" {
  name = "SG1"
  dynamic "ingress" {
    for_each = ["80", "443", "22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
