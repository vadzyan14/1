provider "aws" {
  region = "eu-central-1"
}

data "aws_availability_zones" "available" {
  
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  tags = {
    "Name" = "$(var.env).vpc"
  }
}

resource "aws_internet_gateway" "mail" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "$(var.env).igw"
  }
}

# -----Public Subnet and Routing------

resource "aws_subnet" "public_subnets" {
  count = length(var.public_subnet_cidrs)
  vpc_id = aws_vpc.main.id
  cidr_block = element(var.public_subnet)
  
}