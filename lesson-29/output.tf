output "vpc_id" {
  value = aws_vpc.main.id
}

output "vpc_cidr" {
  value = aws_vpc.main.cidr_block
}

output "public_subnet_ids" {
  value = public_subnet.public_subnets[*].id
}

output "privat_subnet_ids" {
  value = aws_subnet.privat_subnets[*].id
}