provider "aws" {
    region = "eu-central-1"
}

resource "aws_instance" "first_web" {
  ami = "ami-0a1ee2fb28fe05df3"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.dynamicSG.id]
  user_data = templatefile("user_data.sh.tpl", {
      f_name = "Vadim",
        l_name = "Volynets",
        names = ["Mikl", "Villius"]
  })

}